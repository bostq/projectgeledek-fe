<?php include('inc_header.php');?>

<!-- Titlebar
================================================== -->
<div id="titlebar">
	<div class="container">
		<div class="row">
			<div class="col-md-12">

				<h2>Booking</h2>

				<!-- Breadcrumbs -->
				<nav id="breadcrumbs">
					<ul>
						<li><a href="#">Home</a></li>
						<li>Booking</li>
					</ul>
				</nav>

			</div>
		</div>
	</div>
</div>

<!-- Content
================================================== -->

<!-- Container -->
<div class="container">
	<div class="row">

		<!-- Content
		================================================== -->
		<div class="col-lg-8 col-md-8 padding-right-30">

			<h3 class="margin-top-0 margin-bottom-30">Personal Details</h3>

			<div class="row">

				<div class="col-md-6">
					<label>Name</label>
					<input type="text" value="">
				</div>

				<div class="col-md-6">
					<label>Company / Brands Name</label>
					<input type="text" value="">
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>E-Mail Address</label>
						<input type="text" value="">
						<i class="im im-icon-Mail"></i>
					</div>
				</div>

				<div class="col-md-6">
					<div class="input-with-icon medium-icons">
						<label>Phone</label>
						<input type="text" value="">
						<i class="im im-icon-Phone-2"></i>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="col-md-12">
					<h4 class="headline margin-top-0 margin-bottom-30">Tabs Style 1</h4>

					<div class="style-1 book-tab">
						<!-- Tabs Navigation -->
						<ul class="tabs-nav">
							<li class="active">
								<img src="images/material/001-credit-carde.png" alt = "">
								<a href="#tab1b">Book Now</a>
							</li>
							<li>
								<img src="images/material/002-folder.png" alt = "">
								<a href="#tab2b">Request Proposal</a>
							</li>
							<li>
								<img src="images/material/people.png" alt = "">
								<a href="#tab3b">Request Meeting</a>
							</li>
						</ul>
						<!-- Tabs Content -->
						<div class="tabs-container">
							<div class="tab-content" id="tab1b">
								<div class="payment">
									<div class="payment-tab payment-tab-active">
										<div class="payment-tab-trigger">
											<input checked id="mandiri" name="cardType" type="radio" value="paypal">
											<label for="mandiri">Mandiri Virtual Account</label>
											<img class="payment-logo paypal" src="https://i.imgur.com/ApBxkXU.png" alt="">
										</div>

										<div class="payment-tab-content">
											<div class="row">
												<div class="col-md-12">
													<label>Card Number</label>
													<input type="text" value="">
												</div>
												<div class="col-md-12">
													<label>Name on Card</label>
													<input type="text" value="">
												</div>
												<div class="col-md-6">
													<label>CCV Code</label>
													<input type="password" value="" placeholder = "***">
												</div>
												<div class="col-md-6">
													<label>Expiration Date</label>
													<input type="text" value="" placeholder = "MM / YY">
												</div>
											</div>
										</div>
									</div>
									<div class="payment-tab">
										<div class="payment-tab-trigger">
											<input id="bca" name="cardType" type="radio" value="paypal">
											<label for="bca">BCA Virtual Account</label>
											<img class="payment-logo paypal" src="https://i.imgur.com/ApBxkXU.png" alt="">
										</div>

										<div class="payment-tab-content">
											<div class="row">
												<div class="col-md-12">
													<label>Card Number</label>
													<input type="text" value="">
												</div>
												<div class="col-md-12">
													<label>Name on Card</label>
													<input type="text" value="">
												</div>
												<div class="col-md-6">
													<label>CCV Code</label>
													<input type="password" value="" placeholder = "***">
												</div>
												<div class="col-md-6">
													<label>Expiration Date</label>
													<input type="text" value="" placeholder = "MM / YY">
												</div>
											</div>
										</div>
									</div>
									<div class="payment-tab">
										<div class="payment-tab-trigger">
											<input type="radio" name="cardType" id="creditCart" value="creditCard">
											<label for="creditCart">Visa / Mastercard Credit Card</label>
											<img class="payment-logo" src="https://i.imgur.com/IHEKLgm.png" alt="">
										</div>

										<div class="payment-tab-content">
											<div class="row">
												<div class="col-md-12">
													<label>Card Number</label>
													<input type="text" value="">
												</div>
												<div class="col-md-12">
													<label>Name on Card</label>
													<input type="text" value="">
												</div>
												<div class="col-md-6">
													<label>CCV Code</label>
													<input type="password" value="" placeholder = "***">
												</div>
												<div class="col-md-6">
													<label>Expiration Date</label>
													<input type="text" value="" placeholder = "MM / YY">
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="txt-center">
									<br/>
									<div class="terms checkboxes">
										<input id="check-a" type="checkbox" name="check">
										<label for="check-a">I have read &amp; agree to terms and conditions</label>
									</div>
									<br/>
									<a href="thankyou.php" class="button booking-confirmation-btn">Confirm and Pay</a>
								</div>
							</div>
							<div class="tab-content" id="tab2b">
								<h3>Email Address</h3>
								<div class="row">
									<div class="col-md-12">
										<label>Email to</label>
										<input type="email" value="">
									</div>
									<div class="col-md-12">
										<label>cc</label>
										<select data-placeholder="Select Multiple Items" class="chosen-select" multiple="" style="display: none;">
											<option>Item 1</option>
											<option>Item 2</option>
											<option>Item 3</option>
											<option>Item 4</option>
										</select>
									</div>
								</div>
								<div class="txt-center">
									<br/>
									<div class="terms checkboxes">
										<input id="check-b" type="checkbox" name="check">
										<label for="check-b">I have read &amp; agree to terms and conditions</label>
									</div>
									<br/>
									<a href="thankyou.php" class="button booking-confirmation-btn">Sent</a>
								</div>
							</div>
							<div class="tab-content" id="tab3b">
								<div class="row">
									<div class="col-md-7">
										<label>Meeting Place</label>
										<input type="email" value="">
									</div>
									<div class="col-md-5">
										<div class="locate"> <span class="lnr lnr-map-marker"></span> Locate me</div>
									</div>
									<div class="col-md-6 date-time">
										<label>Date</label>
										<span class="lnr lnr-calendar-full"></span>
										<input type="text" value="">
									</div>
									<div class="col-md-6 date-time">
										<label>Time</label>
										<span class="lnr lnr-clock"></span>
										<input type="text" value="">
									</div>
								</div>
								<div class="txt-center">
									<br/>
									<div class="terms checkboxes">
										<input id="check-b" type="checkbox" name="check">
										<label for="check-b">I have read &amp; agree to terms and conditions</label>
									</div>
									<br/>
									<a href="thankyou.php" class="button booking-confirmation-btn">Sent</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-0 margin-bottom-60">

			<!-- Booking Summary -->
			<div class="listing-item-container compact order-summary-widget">
				<div class="listing-item">
					<img src="images/content/side_booked.jpg" alt="">

					<div class="listing-item-content">
						<h3>#GCK1234B56789</h3>
						<span>Jl. Let. Jend. S. Parman, Palmerah</span>
					</div>
				</div>
			</div>
			<div class="boxed-widget opening-hours summary margin-top-0">
				<h3><i class="fa fa-calendar-check-o"></i> Booking Summary</h3>
				<ul>
					<li>On-Air Schedule <span>19 Nov 2017</span></li>
					<li>On-Air Periode <span>1 Year</span></li>
					<li class="total-costs">Total Cost<span>1.500.000.000 (IDR)</span></li>
				</ul>

			</div>
			<!-- Booking Summary / End -->

		</div>

	</div>
</div>
<!-- Container / End -->


<!-- end of middle -->
<?php include('inc_footer.php');?>