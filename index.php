<?php include('inc_header.php');?>
<!-- middle -->

<div class="main-search-container dark-overlay">
	<div class="main-search-inner">

		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>Find Nearby Attractions</h2>
					<h4>Expolore top-rated attractions, activities and more</h4>

					<div class="main-search-input">

						<div class="main-search-input-item">
							<input type="text" placeholder="Location" value=""/>
						</div>
						<div class="main-search-input-item tsleft homeinput">
                            <span class="lnr lnr-calendar-full icwpsleft"></span>
                            <input type="text" id="booking-date2" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2018" placeholder="On-Air Schedule">
                        </div>


						<div class="main-search-input-item">
							<select data-placeholder="All Categories" class="chosen-select" >
								<option>All Categories</option>	
								<option>Shops</option>
								<option>Hotels</option>
								<option>Restaurants</option>
								<option>Fitness</option>
								<option>Events</option>
							</select>
						</div>

						<button class="button">Search</button>

					</div>
				</div>
			</div>
		</div>

	</div>
	
	<!-- Video -->
	<div class="video-container">
		<video poster="images/main-search-video-poster.jpg" loop autoplay muted>
			<source src="images/main-search-video.mp4" type="video/mp4">
		</video>
	</div>

</div>



<!-- Content
================================================== -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-top-75">
				Browse Categories
			</h3>
		</div>

	</div>
</div>


<!-- Category Boxes -->
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="categories-boxes-container inlinecenterbox margin-bottom-30">
				
				<!-- Box -->
				<a href="catalog.php" class="category-small-box">
					<i class="im im-icon-Hamburger"></i>
					<h4>Outdoor</h4>
				</a>

				<!-- Box -->
				<a href="catalog.php" class="category-small-box">
					<i class="im  im-icon-Sleeping"></i>
					<h4>Indoor</h4>
				</a>

				<!-- Box -->
				<a href="catalog.php" class="category-small-box comingsoon">
					<i class="im im-icon-Shopping-Bag"></i>
					<h4>Mobile</h4>
				</a>

				<!-- Box -->
				<a href="catalog.php" class="category-small-box comingsoon">
					<i class="im im-icon-Cocktail"></i>
					<h4>Broadcast</h4>
				</a>

			</div>
		</div>
	</div>
</div>
<!-- Category Boxes / End -->


<!-- Fullwidth Section -->
<section class="fullwidth margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f8f8f8">

	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-45">
					Most Visited Media
					<span>Discover top-rated media</span>
				</h3>
			</div>
		</div>
	</div>

	<!-- Carousel / Start -->
	<div class="simple-fw-slick-carousel dots-nav">

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->		

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->

		<!-- Listing Item -->
		<div class="fw-carousel-item">
			<div class="smslide">
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
				<div class="lssmslide">
					<a href="detail.php" class="listing-item-container compact">
						<div class="listing-item">
							<img src="images/listing-item-02.jpg" alt="">
							<div class="listing-item-content">
								<span class="tag taglg">led videotron</span>
								<h3>Sticky Band 2</h3>
								<span>Bishop Avenue, New York</span>
							</div>
						</div>
					</a>
				</div>
			</div>
		</div>
		<!-- Listing Item / End -->

	</div>
	<!-- Carousel / End -->


</section>
<!-- Fullwidth Section / End -->


<!-- Container -->
<div class="container">
	<div class="row">

		<div class="col-md-12">
			<h3 class="headline centered margin-bottom-35 margin-top-70">Popular Cities <span>Browse listings in popular places</span></h3>
		</div>
		
		<div class="col-md-4">

			<!-- Image Box -->
			<a href="catalog.php" class="img-box" data-background-image="images/popular-location-01.jpg">
				<div class="img-box-content visible">
					<h4>New York </h4>
					<span>14 Listings</span>
				</div>
			</a>

		</div>	
			
		<div class="col-md-8">

			<!-- Image Box -->
			<a href="catalog.php" class="img-box" data-background-image="images/popular-location-02.jpg">
				<div class="img-box-content visible">
					<h4>Los Angeles</h4>
					<span>24 Listings</span>
				</div>
			</a>

		</div>	

		<div class="col-md-8">

			<!-- Image Box -->
			<a href="catalog.php" class="img-box" data-background-image="images/popular-location-03.jpg">
				<div class="img-box-content visible">
					<h4>San Francisco </h4>
					<span>12 Listings</span>
				</div>
			</a>

		</div>	
			
		<div class="col-md-4">

			<!-- Image Box -->
			<a href="catalog.php" class="img-box" data-background-image="images/popular-location-04.jpg">
				<div class="img-box-content visible">
					<h4>Miami</h4>
					<span>9 Listings</span>
				</div>
			</a>

		</div>

	</div>
</div>
<!-- Container / End -->


<!-- Info Section -->
<div class="container">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="headline centered margin-top-80 ttlelse">
				Plan The Media Buying From Where You Are
				<span class="margin-top-25">Explore some of the best media from around are the country from our partners and friends.  Discover some of the most popular listing in Indonesia.</span>
			</h2>
		</div>
	</div>

	<div class="row icons-container">
		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="im im-icon-Map2"></i>
				<h3>Find It</h3>
				<p>You need to go anywhere to choose a thousand options of the space you desired</p>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2 with-line">
				<i class="icwp ic_business"></i>
				<h3>Get it</h3>
				<p>Get the business proposal and you can booked directly on our website.</p>
			</div>
		</div>

		<!-- Stage -->
		<div class="col-md-4">
			<div class="icon-box-2">
				<i class="icwp ic_handshow"></i>
				<h3>Show it</h3>
				<p>Prepare the advertising material and let's buzz your messages.</p>
			</div>
		</div>
	</div>

</div>
<!-- Info Section / End -->


<!-- Flip banner -->
<a href="catalog.php" class="flip-banner parallax margin-top-65" data-background="images/slider-bg-02.jpg" data-color="#f91942" data-color-opacity="0.85" data-img-width="2500" data-img-height="1666">
	<div class="flip-banner-content">
		<h2 class="flip-visible">Expolore top-rated attractions nearby</h2>
		<h2 class="flip-hidden">Browse Listings <i class="sl sl-icon-arrow-right"></i></h2>
	</div>
</a>
<!-- Flip banner / End -->



<!-- Info Section -->
<div class="container">

	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<h2 class="headline centered margin-top-80 ttlelse">
				Why Choose <i class="pinkc">Piece A Cakes</i>
				<span class="margin-top-25">Have you been frustrated to seek the best advertising media?<br>
					You come to the right place. We will make you work easier, faster and <br>
					more reliable from where you are.</span>
			</h2>
		</div>
	</div>

	<div class="pac-container">
		<div class="ls-pac">
			<div class="in-pac">
				<span class="numpac">01</span>
				<span class="lnr lnr-earth"></span>
				<h3>Hugo Choices</h3>
				<p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
			</div>
		</div>
		<div class="ls-pac">
			<div class="in-pac">
				<span class="numpac">02</span>
				<span class="lnr lnr-chart-bars"></span>
				<h3>Measureable</h3>
				<p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
			</div>
		</div>
		<div class="ls-pac">
			<div class="in-pac">
				<span class="numpac">03</span>
				<span class="lnr lnr-smile"></span>
				<h3>Fast & Friendly</h3>
				<p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
			</div>
		</div>
		<div class="ls-pac">
			<div class="in-pac">
				<span class="numpac">04</span>
				<span class="lnr lnr-checkmark-circle"></span>
				<h3>It’s Easy</h3>
				<p>Nunc placerat mi id nisi interdum mollis. Praesent pharetra, justo ut scelerisque the mattis, leo quam aliquet diam congue is laoreet elit metus.</p>
			</div>
		</div>
	</div>

</div>
<!-- Info Section / End -->


<section class="fullwidth border-top margin-top-40 margin-bottom-0 padding-top-60 padding-bottom-65" data-background-color="#ffffff">
	<!-- Logo Carousel -->
	<div class="container">
		<div class="row">

			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-40 margin-top-10">Companies We've Worked With <span>We can assist you with your innovation or commercialisation journey!</span></h3>
			</div>
			
			<!-- Carousel -->
			<div class="col-md-12">
				<div class="logo-slick-carousel dot-navigation">
					
					<div class="item">
						<img src="images/logo-01.png" alt="">
					</div>
					
					<div class="item">
						<img src="images/logo-02.png" alt="">
					</div>
					
					<div class="item">
						<img src="images/logo-03.png" alt="">
					</div>
					
					<div class="item">
						<img src="images/logo-04.png" alt="">
					</div>
					
					<div class="item">
						<img src="images/logo-05.png" alt="">
					</div>		

					<div class="item">
						<img src="images/logo-06.png" alt="">
					</div>	

					<div class="item">
						<img src="images/logo-07.png" alt="">
					</div>


				</div>
			</div>
			<!-- Carousel / End -->

		</div>
	</div>
	<!-- Logo Carousel / End -->
</section>


<!-- Recent Blog Posts -->
<section class="fullwidth margin-top-70 padding-bottom-75" data-background-color="#fff">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<h3 class="headline centered margin-bottom-45">
					From The Blog
				</h3>
			</div>
		</div>

		<div class="row">
			<!-- Blog Post Item -->
			<div class="col-md-4">
				<a href="#" class="blog-compact-item-container">
					<div class="blog-compact-item">
						<img src="images/blog-compact-post-01.jpg" alt="">
						<span class="blog-item-tag">Tips</span>
						<div class="blog-compact-item-content">
							<ul class="blog-post-tags">
								<li>22 August 2017</li>
							</ul>
							<h3>Hotels for All Budgets</h3>
							<p>Sed sed tristique nibh iam porta volutpat finibus. Donec in aliquet urneget mattis lorem. Pellentesque pellentesque.</p>
						</div>
					</div>
				</a>
			</div>
			<!-- Blog post Item / End -->

			<!-- Blog Post Item -->
			<div class="col-md-4">
				<a href="#" class="blog-compact-item-container">
					<div class="blog-compact-item">
						<img src="images/blog-compact-post-02.jpg" alt="">
						<span class="blog-item-tag">Tips</span>
						<div class="blog-compact-item-content">
							<ul class="blog-post-tags">
								<li>18 August 2017</li>
							</ul>
							<h3>The 50 Greatest Street Arts In London</h3>
							<p>Sed sed tristique nibh iam porta volutpat finibus. Donec in aliquet urneget mattis lorem. Pellentesque pellentesque.</p>
						</div>
					</div>
				</a>
			</div>
			<!-- Blog post Item / End -->

			<!-- Blog Post Item -->
			<div class="col-md-4">
				<a href="#" class="blog-compact-item-container">
					<div class="blog-compact-item">
						<img src="images/blog-compact-post-03.jpg" alt="">
						<span class="blog-item-tag">Tips</span>
						<div class="blog-compact-item-content">
							<ul class="blog-post-tags">
								<li>10 August 2017</li>
							</ul>
							<h3>The Best Cofee Shops In Sydney Neighborhoods</h3>
							<p>Sed sed tristique nibh iam porta volutpat finibus. Donec in aliquet urneget mattis lorem. Pellentesque pellentesque.</p>
						</div>
					</div>
				</a>
			</div>
			<!-- Blog post Item / End -->

			<div class="col-md-12 centered-content">
				<a href="#" class="button border margin-top-10">View Blog</a>
			</div>

		</div>

	</div>
</section>
<!-- Recent Blog Posts / End -->

<!-- end of middle -->
<?php include('inc_footer.php');?>

<script>$('#booking-date2').dateDropper();</script> 