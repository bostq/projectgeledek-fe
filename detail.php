<?php include('inc_header.php');?>
<!-- middle -->
<div class="listing-slider mfp-gallery-container margin-bottom-0">
	<a href="images/content/ban_det_1.jpg" data-background-image="images/content/ban_det_1.jpg" class="item mfp-gallery" title="Title 1"></a>
	<a href="images/content/ban_det_2.jpg" data-background-image="images/content/ban_det_2.jpg" class="item mfp-gallery" title="Title 3"></a>
	<a href="images/content/ban_det_1.jpg" data-background-image="images/content/ban_det_1.jpg" class="item mfp-gallery" title="Title 2"></a>
	<a href="images/content/ban_det_2.jpg" data-background-image="images/content/ban_det_2.jpg" class="item mfp-gallery" title="Title 4"></a>
</div>

<!-- Content
================================================== -->
<div class="container">
	<div class="row sticky-wrapper">
		<div class="col-lg-8 col-md-8 padding-right-30">
			<!-- Location -->
			<div id="listing-location" class="listing-section">
				<h3 class="listing-desc-headline margin-top-60 margin-bottom-30">Location</h3>

				<div id="singleListingMap-container">
					<div id="singleListingMap" data-latitude="40.70437865245596" data-longitude="-73.98674011230469" data-map-icon="im im-icon-Hamburger"></div>
					<a href="#" id="streetView">Street View</a>
				</div>
			</div>

			<div class="listing-section det-address">
				<div class="head">
					<span class = "big">#CGK1234A1234</span><span class = "billb">Billboard</span>
				</div>
				<div class="cont">
					<span></span>
					<p>Jl. Let. Jend. S. Parman Kav. 73, Pal Merah - Pal Merah <br/> Jakarta Barat - DKI Jakarta</p>
				</div>
			</div>
			
			<!-- Listing Nav -->
			<div id="listing-nav" class="listing-nav-container">
				<ul class="listing-nav">
					<li><a href="#listing-overview" class="active">Overview</a></li>
				</ul>
			</div>
			
			<!-- Overview -->
			<div id="listing-overview" class="listing-section">

				<!-- Description -->
				<p>
					Ut euismod ultricies sollicitudin. Curabitur sed dapibus nulla. Nulla eget iaculis lectus. Mauris ac maximus neque. Nam in mauris quis libero sodales eleifend. Morbi varius, nulla sit amet rutrum elementum, est elit finibus tellus, ut tristique elit risus at metus.
				</p>

				<div class="det-feature">
					<div class = "list">
						<span class = "f1"></span>
						<p>Vertical</p>
					</div>
					<div class = "list">
						<span class = "f2"></span>
						<p>8m x 16m</p>
					</div>
					<div class = "list">
						<span class = "f3"></span>
						<p>Back Light</p>
					</div>
					<div class = "list">
						<span class = "f4"></span>
						<p>Single board (1 face)</p>
					</div>
				</div>

				<!-- Price Includes -->
				<h3 class="listing-desc-headline">Price Includes</h3>
				<ul class="listing-features checkboxes margin-top-0">
					<li>Media plactement</li>
					<li>Advertising tax</li>
					<li>Maintenance</li>
					<li>Advertising Permit</li>
					<li>Land Lease</li>
					<li>Insurance</li>
					<li>Electric Power</li>
					<li>Banner Printing</li>
				</ul>

				<!-- Price Excludes -->
				<h3 class="listing-desc-headline">Price Excludes</h3>
				<ul class="listing-features closeboxes">
					<li>PPn Tax</li>
					<li>Customize creative production</li>
				</ul>
			</div>
		</div>

		<!-- Sidebar
		================================================== -->
		<div class="col-lg-4 col-md-4 margin-top-75 sticky">

				
			<!-- Verified Badge -->
			<div class="verified-badge with-tip" data-tip-content="Listing has been verified and belongs the business owner or manager.">
				<i class="sl sl-icon-check"></i> Verified Listing
			</div>

			<!-- Book Now -->
			<div class="boxed-widget booking-widget margin-top-35">
				<div class="price-cont">
					<span class = "price">1.500.000.000 (IDR)</span>
					<span>per year</span>
				</div>
				<h3><i class="fa fa-calendar-check-o "></i> Book this spot</h3>
				<div class="row with-forms  margin-top-0">

					<!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
					<div class="col-lg-6 col-md-12">
						<input type="text" id="booking-date-1" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2020" data-disabled-days="08/17/2017,08/18/2017">
					</div>

					<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
					<div class="col-lg-6 col-md-12">
						<input type="text" id="booking-date-2" data-lang="en" data-large-mode="true" data-large-default="true" data-min-year="2017" data-max-year="2020" data-disabled-days="08/17/2017,08/18/2017">
					</div>
				</div>
				
				<!-- progress button animation handled via custom.js -->
				<a href="booked.php" class="button book-now fullwidth margin-top-5">Book Now</a>
			</div>
			<!-- Book Now / End -->

			<!-- Contact -->
			<div class="boxed-widget margin-top-35 account">
				<div class="popup-premium">
					<div class="inner">
						<p>Be Premium Account to see full contact detail of the media owners.</p>
						<a href="#" class="button book-now fullwidth margin-top-5">Get Premium Account</a>
					</div>
				</div>
				<div class="non">
					<div class="hosted-by-title">
						<h4><span>Hosted by</span> <a href="pages-user-profile.html">Tom Perrin</a></h4>
						<a href="pages-user-profile.html" class="hosted-by-avatar"><img src="images/dashboard-avatar.jpg" alt=""></a>
					</div>
					<ul class="listing-details-sidebar">
						<li><i class="sl sl-icon-phone"></i> (123) 123-456</li>
						<!-- <li><i class="sl sl-icon-globe"></i> <a href="#">http://example.com</a></li> -->
						<li><i class="fa fa-envelope-o"></i> <a href="#">tom@example.com</a></li>
					</ul>
					<ul class="listing-details-sidebar social-profiles">
						<li><a href="#" class="facebook-profile"><i class="fa fa-facebook-square"></i> Facebook</a></li>
						<li><a href="#" class="twitter-profile"><i class="fa fa-twitter"></i> Twitter</a></li>
						<!-- <li><a href="#" class="gplus-profile"><i class="fa fa-google-plus"></i> Google Plus</a></li> -->
					</ul>
					<!-- Reply to review popup -->
					<div id="small-dialog" class="zoom-anim-dialog mfp-hide">
						<div class="small-dialog-header">
							<h3>Send Message</h3>
						</div>
						<div class="message-reply margin-top-0">
							<textarea cols="40" rows="3" placeholder="Your message to Tom"></textarea>
							<button class="button">Send Message</button>
						</div>
					</div>
					<a href="#small-dialog" class="send-message-to-owner button popup-with-zoom-anim"><i class="sl sl-icon-envelope-open"></i> Send Message</a>
				</div>
			</div>
			<!-- Contact / End-->


			<!-- Share / Like -->
			<div class="listing-share margin-top-40 margin-bottom-40 no-border">
				<button class="like-button"><span class="like-icon"></span> Bookmark this listing</button> 
				<button class="like-button"><span class="lnr lnr-download"></span> Download this listing</button> 
				<!-- Share Buttons -->
				<ul class="share-buttons margin-top-40 margin-bottom-0">
					<li><a class="fb-share" href="#"><i class="fa fa-facebook"></i> Share</a></li>
					<li><a class="twitter-share" href="#"><i class="fa fa-twitter"></i> Tweet</a></li>
					<li><a class="gplus-share" href="#"><i class="fa fa-google-plus"></i> Share</a></li>
					<!-- <li><a class="pinterest-share" href="#"><i class="fa fa-pinterest-p"></i> Pin</a></li> -->
				</ul>
				<div class="clearfix"></div>
			</div>

		</div>
		<!-- Sidebar / End -->
	</div>
</div>

<!-- end of middle -->
<?php include('inc_footer.php');?>

<script>$('#booking-date-1').dateDropper();</script> 
<script>$('#booking-date-2').dateDropper();</script> 

<!-- Time Picker - docs: http://www.vasterad.com/docs/listeo/#!/time_picker -->
<script src="scripts/timedropper.js"></script>
<link rel="stylesheet" type="text/css" href="css/plugins/timedropper.css"> 
<script>
var $clocks = $('.td-input');
	_.each($clocks, function(clock){
	clock.value = null;
});