<?php include('inc_header.php');?>
<!-- middle -->
<!-- Content
================================================== -->
<div class="fs-container">
	<div class="fs-inner-container content">
		<div class="fs-content">
			<!-- Search -->
			<section class="search">

				<div class="row">
					<div class="col-md-12">

							<!-- Row With Forms -->
							<div class="row with-forms">

								<!-- Filters -->
								<div class="col-fs-12">

									<!-- Panel Dropdown / End -->
									<div class="panel-dropdown">
										<a href="#">Price Range</a>
										<div class="panel-dropdown-content">
											<input class="distance-radius" type="range" min="0" max="250000000" step="1000000" value="125000000" data-title="Media cost per year">
											<div class="panel-buttons">
												<button class="panel-cancel">Disable</button>
												<button class="panel-apply">Apply</button>
											</div>
										</div>
									</div>
									<!-- Panel Dropdown / End -->

									<!-- Panel Dropdown -->
									<div class="panel-dropdown wide">
										<a href="#">More Filters</a>
										<div class="panel-dropdown-content checkboxes">

											<!-- Checkboxes -->
											<div class="row">
												<div class="col-md-6">
													<input id="check-a" type="checkbox" name="check">
													<label for="check-a">Front Light</label>

													<input id="check-b" type="checkbox" name="check">
													<label for="check-b">Back Light</label>

													<input id="check-c" type="checkbox" name="check">
													<label for="check-c">Single Board 1(face)</label>

													<input id="check-d" type="checkbox" name="check">
													<label for="check-d">Double Board 2(face)</label>

													<input id="check-e" type="checkbox" name="check">
													<label for="check-e">Horizontal</label>

													<input id="check-f" type="checkbox" name="check">
													<label for="check-f">Vertical</label>
												</div>	

												<div class="col-md-6">
													<input id="check-g" type="checkbox" name="check" >
													<label for="check-g">30' sec</label>

													<input id="check-h" type="checkbox" name="check" >
													<label for="check-h">60' sec</label>
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-6">
													<input id="check-1" type="checkbox" name="check">
													<label for="check-1">Suburd</label>

													<input id="check-2" type="checkbox" name="check">
													<label for="check-2">School & University</label>

													<input id="check-3" type="checkbox" name="check">
													<label for="check-3">Mall</label>

													<input id="check-4" type="checkbox" name="check">
													<label for="check-4">Office</label>

													<input id="check-5" type="checkbox" name="check">
													<label for="check-5">Transportation Area</label>

												</div>
												<div class="col-md-6">
													<input id="check-6" type="checkbox" name="check">
													<label for="check-6">Apartments</label>

													<input id="check-7" type="checkbox" name="check">
													<label for="check-7">Hospital</label>

													<input id="check-8" type="checkbox" name="check">
													<label for="check-8">Holyplace (Mosque, Church, etc)</label>

													<input id="check-9" type="checkbox" name="check">
													<label for="check-9">Hotel</label>

													<input id="check-10" type="checkbox" name="check">
													<label for="check-10">Parking lot</label>

												</div>	
											</div>
											
											<!-- Buttons -->
											<div class="panel-buttons">
												<button class="panel-cancel">Cancel</button>
												<button class="panel-apply">Apply</button>
											</div>

										</div>
									</div>
									<!-- Panel Dropdown / End -->

									<div class="panel-dropdown wide">
										<a href="#">Targeting</a>
										<div class="panel-dropdown-content checkboxes">

											<!-- Checkboxes -->
											<div class="row">
												<div class="col-md-6 op">
													<input id="check-i" type="checkbox" name="check">
													<label for="check-i"> < 1000</label>

													<input id="check-j" type="checkbox" name="check">
													<label for="check-j"> < 10.000</label>

													<input id="check-k" type="checkbox" name="check">
													<label for="check-k"> < 100.000</label>

													<input id="check-l" type="checkbox" name="check">
													<label for="check-l"> < 1.000.000</label>
												</div>	

												<div class="col-md-6 op">
													<input id="check-m" type="checkbox" name="check" >
													<label for="check-m">12-17</label>

													<input id="check-n" type="checkbox" name="check" >
													<label for="check-n">18-24</label>

													<input id="check-o" type="checkbox" name="check">
													<label for="check-o">25-34</label>	

													<input id="check-p" type="checkbox" name="check">
													<label for="check-p">45-54</label>

													<input id="check-q" type="checkbox" name="check">
													<label for="check-q">55-64</label>

													<input id="check-r" type="checkbox" name="check">
													<label for="check-r">65+</label>
												</div>

												<div class="col-md-6 op">
												
													<input id="check-s" type="checkbox" name="check" >
													<label for="check-s">Male</label>

													<input id="check-t" type="checkbox" name="check" >
													<label for="check-t">Female</label>

												</div>
											</div>
											
											<!-- Buttons -->
											<div class="panel-buttons">
												<button class="panel-cancel">Cancel</button>
												<button class="panel-apply">Apply</button>
											</div>

										</div>
									</div>
									<!-- Panel Dropdown / End -->									
								</div>
								<!-- Filters / End -->
	
							</div>
							<!-- Row With Forms / End -->

					</div>
				</div>

			</section>
			<!-- Search / End -->


		<section class="listings-container margin-top-30">
			<!-- Sorting / Layout Switcher -->
			<div class="row fs-switcher">

				<div class="col-md-6">
					<!-- Showing Results -->
					<p class="showing-results">14 Results Found </p>
				</div>

			</div>

			<!-- Listings -->
			<div class="row fs-listings">
				
				<!-- Listing Item -->
				<div class="col-lg-6 col-md-12">
					<div class="fw-carousel-item">
						<div class="smslide">
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band 2</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="fw-carousel-item">
						<div class="smslide">
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band 2</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="fw-carousel-item">
						<div class="smslide">
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band 2</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-12">
					<div class="fw-carousel-item">
						<div class="smslide">
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
							<div class="lssmslide">
								<a href="detail.php" class="listing-item-container compact">
									<div class="listing-item">
										<img src="images/listing-item-02.jpg" alt="">
										<div class="listing-item-content">
											<span class="tag taglg">led videotron</span>
											<h3>Sticky Band 2</h3>
											<span>Bishop Avenue, New York</span>
										</div>
									</div>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Listing Item / End -->	

			</div>
			<!-- Listings Container / End -->


			<!-- Pagination Container -->
			<div class="row fs-listings">
				<div class="col-md-12">

					<!-- Pagination -->
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12">
							<!-- Pagination -->
							<div class="pagination-container margin-top-15 margin-bottom-40">
								<nav class="pagination">
									<ul>
										<li><a href="#" class="current-page">1</a></li>
										<li><a href="#">2</a></li>
										<li><a href="#">3</a></li>
										<li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
									</ul>
								</nav>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!-- Pagination / End -->
					
					<!-- Copyrights -->
					<div class="copyrights margin-top-0">© 2017 Listeo. All Rights Reserved.</div>

				</div>
			</div>
			<!-- Pagination Container / End -->
		</section>

		</div>
	</div>
	<div class="fs-inner-container map-fixed">

		<!-- Map -->
		<div id="map-container">
		    <div id="map" data-map-zoom="9" data-map-scroll="true">
		        <!-- map goes here -->
		    </div>
		</div>

	</div>
</div>
<!-- Wrapper / End -->

<!-- end of middle -->

<!-- Scripts
================================================== -->
<?php include('inc_js.php');?>


<!-- Maps -->
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyAyMmUbesL5h7QEAcLM4VB8LyPW1LTZErw&callback=initMap"></script>
<script type="text/javascript" src="scripts/infobox.min.js"></script>
<script type="text/javascript" src="scripts/markerclusterer.js"></script>
<script type="text/javascript" src="scripts/maps.js"></script>