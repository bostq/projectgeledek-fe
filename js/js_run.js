/*=============================================================================================	
 Company    : PT Web Architect Technology - webarq.com
 Document   : Javascript Plugin Exe
 Author     : bosq
 ==============================================================================================*/
$(document).ready(function () {
    $(".select2").select2({ //select customize 
        width: '100%'
    });
    $('.smslide').slick({
    	draggable: false,
    	swipeToSlide: false,
    	fade: true
    });
});
