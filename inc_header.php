<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Piece A Cakes</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no"/>

        <link rel="icon" href="favicon.ico">

        <!--Style-->
        <!-- <link rel="stylesheet" href="css/select2.min.css"> -->
        <!--build:css css/styles.min.css--> 

        <link rel="stylesheet" href="css/styletemplate.css">
        <link rel="stylesheet" href="css/colors/main.css" id="colors">
        <!-- <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css"> -->
        <link rel="stylesheet" href="fonts/Linearicons/Web Font/style.css">

        <!-- Date Picker - docs: http://www.vasterad.com/docs/listeo/#!/date_picker -->
        <link href="css/plugins/datedropper.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/style_fuja.css">
        <link rel="stylesheet" href="css/style-agha.css">
        <!--endbuild-->
        
        <!--js-->
        <?php /* <!--build:js js/main.min.js -->
        <!-- <script type="text/javascript" src="js/jquery-1.11.2.min.js"></script> -->
        <!--endbuild--> */ ?>

    </head>
    <body>


        <!-- Wrapper -->
        <div id="wrapper">

        <!-- Header Container
        ================================================== -->
        <header id="header-container">

            <!-- Header -->
            <div id="header">
                <div class="container">
                    
                    <!-- Left Side Content -->
                    <div class="left-side">
                        
                        <!-- Logo -->
                        <div id="logo">
                            <a href="index.php"><img src="images/material/logo.png" alt="logo PAC"></a>
                        </div>

                        <div class="main-search-input">

                            <div class="main-search-input-item tsleft">
                                <span class="lnr lnr-magnifier icwpsleft"></span>
                                <input type="text" placeholder="Location" value=""/>
                            </div>
                            <div class="main-search-input-item tsleft">
                                <span class="lnr lnr-calendar-full icwpsleft"></span>
                                <input type="text" id="booking-date" data-lang="en" data-min-year="2017" data-max-year="2018" placeholder="On-Air Schedule">
                            </div>
                            <div class="main-search-input-item">
                                <select data-placeholder="All Categories" class="chosen-select" >
                                    <option>All Categories</option> 
                                    <option>Shops</option>
                                    <option>Hotels</option>
                                    <option>Restaurants</option>
                                    <option>Fitness</option>
                                    <option>Events</option>
                                </select>
                            </div>

                            <button class="button"><span class="lnr lnr-magnifier"></span></button>

                        </div>

                        <!-- Mobile Navigation -->
                        <div class="mmenu-trigger">
                            <button class="hamburger hamburger--collapse" type="button">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>

                        <!-- Main Navigation -->
                        <nav id="navigation" class="style-1">
                            <ul id="responsive">

                                <li><a class="current" href="#">Categories</a>
                                    <ul>
                                        <li><a href="catalog.php">Shop</a></li>
                                        <li><a href="catalog.php">Hotel</a></li>
                                        <li><a href="catalog.php">Fitness</a></li>
                                        <li><a href="catalog.php">Events</a></li>
                                    </ul>
                                </li>

                                <li><a href="#">How it work</a></li>
                                <li><a href="#">About us</a></li>
                                <li><a href="#">Help</a></li>
                            </ul>
                        </nav>
                        <div class="clearfix"></div>
                        <!-- Main Navigation / End -->
                        
                    </div>
                    <!-- Left Side Content / End -->


                    <!-- Right Side Content / End -->
                    <div class="right-side">
                        <div class="header-widget">
                            <a href="#" class="button border with-icon">Add Listing <i class="sl sl-icon-plus"></i></a>
                            <a href="#sign-in-dialog" class="sign-in popup-with-zoom-anim"><i class="sl sl-icon-login"></i> Sign In</a>
                        </div>
                    </div>
                    <!-- Right Side Content / End -->

                    <!-- Sign In Popup -->
                    <div id="sign-in-dialog" class="zoom-anim-dialog mfp-hide">

                        <div class="small-dialog-header">
                            <h3>Sign In</h3>
                        </div>

                        <!--Tabs -->
                        <div class="sign-in-form style-1">

                            <ul class="tabs-nav">
                                <li class=""><a href="#tab1">Log In</a></li>
                                <li><a href="#tab2">Register</a></li>
                            </ul>

                            <div class="tabs-container alt">

                                <!-- Login -->
                                <div class="tab-content" id="tab1" style="display: none;">
                                    <form method="post" class="login">

                                        <p class="form-row form-row-wide">
                                            <label for="username">Username:
                                                <i class="im im-icon-Male"></i>
                                                <input type="text" class="input-text" name="username" id="username" value="" />
                                            </label>
                                        </p>

                                        <p class="form-row form-row-wide">
                                            <label for="password">Password:
                                                <i class="im im-icon-Lock-2"></i>
                                                <input class="input-text" type="password" name="password" id="password"/>
                                            </label>
                                            <span class="lost_password">
                                                <a href="#" >Lost Your Password?</a>
                                            </span>
                                        </p>

                                        <div class="form-row">
                                            <input type="submit" class="button border margin-top-5" name="login" value="Login" />
                                            <div class="checkboxes margin-top-10">
                                                <input id="remember-me" type="checkbox" name="check">
                                                <label for="remember-me">Remember Me</label>
                                            </div>
                                        </div>
                                        
                                    </form>
                                </div>

                                <!-- Register -->
                                <div class="tab-content" id="tab2" style="display: none;">

                                    <form method="post" class="register">
                                        
                                    <p class="form-row form-row-wide">
                                        <label for="username2">Username:
                                            <i class="im im-icon-Male"></i>
                                            <input type="text" class="input-text" name="username" id="username2" value="" />
                                        </label>
                                    </p>
                                        
                                    <p class="form-row form-row-wide">
                                        <label for="email2">Email Address:
                                            <i class="im im-icon-Mail"></i>
                                            <input type="text" class="input-text" name="email" id="email2" value="" />
                                        </label>
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <label for="password1">Password:
                                            <i class="im im-icon-Lock-2"></i>
                                            <input class="input-text" type="password" name="password1" id="password1"/>
                                        </label>
                                    </p>

                                    <p class="form-row form-row-wide">
                                        <label for="password2">Repeat Password:
                                            <i class="im im-icon-Lock-2"></i>
                                            <input class="input-text" type="password" name="password2" id="password2"/>
                                        </label>
                                    </p>

                                    <input type="submit" class="button border fw margin-top-10" name="register" value="Register" />
            
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Sign In Popup / End -->

                </div>
            </div>
            <!-- Header / End -->

        </header>
        <div class="clearfix"></div>
        <!-- Header Container / End -->